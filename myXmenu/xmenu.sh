#!/bin/sh

dat=$(date +'%d/%m/%Y')
time=$(date +'%H:%M')
day=$(date +'%A')
BATTERY_POWER=`cat /sys/class/power_supply/BAT0/capacity`
battery_prompt () {
    BATTERY_POWER=`cat /sys/class/power_supply/BAT0/capacity`
    [ $BATTERY_POWER -eq 100 ] \
        && echo "Full ,$BATTERY_POWER% "
    [ $BATTERY_POWER -lt 99 ] && [ $BATTERY_POWER -ge 15 ] \
        && echo "Norm, $BATTERY_POWER% "
    [ $BATTERY_POWER -lt 15 ] \
        && echo "Low, $BATTERY_POWER% "
}
xmenu -i -p 12x12:0 <<EOF |  sh &
`battery_prompt`
$time	
	$dat
	$day

Applications
	Firefox		firefox
	Terminal	alacritty
Emacs	
	restart		systemctl --user restart emacs.service
	emacs		emacsclient -c -a emacs
Bluetooth	pavucontrol ; alacritty -e bluetoothctl
search
	google		/home/vamshi/myDmenu/google
	youtube		/home/vamshi/myDmenu/youtube
Redshift
	restart		systemctl --user restart redshift.service
	open		alacritty -e  nvim ~/.config/redshift/redshift.conf
	stop		systemctl --user stop redshift.service
XMonad	
	restart		xmonad --recompile ; xmonad --restart
	doc		firefox https://hackage.haskell.org/package/xmonad-contrib-0.16/docs/XMonad-Doc-Extending.html
Nixos
	store		/home/vamshi/aios/dmenu/nixos
XMenu	alacritty -e nvim ~/myXmenu/xmenu.sh
Yay	~/myDmenu/yay.sh
Git
	vamshi5070	firefox "gitlab.com/vamshi5070"
	dt		firefox "gitlab.com/dwt1/dotfiles"
EOF

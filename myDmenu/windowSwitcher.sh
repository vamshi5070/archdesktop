num=$(wmctrl -l | cut -d ' ' -f5- | sed -r 's/([0-9]+)+/ /g'| nl -w 1 -n rn | dmenu -n -l 10 -sb "#dc55a0" -fn "IosevkaNerdFont:size=34" |  cut -b 1)
[[ -z "$num" ]] && exit
wmctrl -l | sed -n "$num p" | cut -c -10 | xargs wmctrl -i -a

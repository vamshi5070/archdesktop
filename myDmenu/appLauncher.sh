#!/usr/bin/env bash

redshifT() {
	
	choice=$(echo -e "1 - restart\n2 - stop" | dmenu -l 10 -fn "Iosevka Nerd Font:size=45" -p "${bat},${time}" -n -ix | cut -b 1)
	case $choice in
		1)
			systemctl --user restart redshift.service
			;;
		2)
			systemctl --user stop redshift.service
			;;
	esac
}

	
dat=$(date)
bat=`cat /sys/class/power_supply/BAT0/capacity`
time=$(date +'%H:%M')
choice=$(echo -e "1 - app\n2 - redshift" | dmenu -l 10 -fn "Iosevka Nerd Font:size=45" -p "${bat},${time}" -n -ix | cut -b 1)
echo $choice

case $choice in
	2)
		redshifT
		;;
	*)
		echo -e "xmonadsds"
		;;
esac




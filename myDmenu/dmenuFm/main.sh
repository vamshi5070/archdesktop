#!/bin/sh

mvToTrash () {
    mv $1 /home/vamshi/.trash/
}
apps="0 - lavda\n1 - Emacs\n2 - Zathura\n3 - Move to Trash\n4 - Neovim\n5 - Just run"
home="/home/vamshi/"
subFun (){
    dir=$1
res=$(ls -aF $dir | dmenu -fn "Iosevka Nerd Font:size=23" -sf "black" -sb "white" -nb "black" -nf "white" -i -l 9 -p $dir -g 5 -n)
case $res in
    ./)
        resCat=$dir
        echo "1"
        ;;
    ../)
        resCat=$(echo -e $dir | rev | cut --complement -d '/' -f 2 | rev)
        echo "2"
        ;;
    "")
        exit 1
        ;;

    *)
        resCat="${dir}${res}"
        # echo "default"
        ;;
esac
if [ -d $resCat ]; then
    functio $resCat
fi

}
# [ -d "/path/dir/" ]
functio () {
dir=$1
res=$(ls -aF $dir | dmenu -fn "Iosevka Nerd Font:size=23" -sf "black" -sb "white" -nb "black" -nf "white" -i -l 9 -p $dir -g 5 -n )
case $res in
    ./)
        resCat=$dir
        echo "1"
        ;;
    ../)
        resCat=$(echo -e $dir | rev | cut --complement -d '/' -f 2 | rev)
        echo "2"
        ;;
    "")
        exit 1
        ;;

    *)
        resCat="${dir}${res}"
        # echo "default"
        ;;
esac
# if [[ $res = *[!\ ]* ]]; then
#     exit 1
# fi
if [ -d $resCat ]; then
    functio $resCat
    # echo "'$res' "
else
    choice=$(echo -e $apps | dmenu -fn "Iosevka Nerd Font:size=23" -sf "black" -sb "#f0f0f0" -nb "black" -nf "white" -i -p $resCat -l 14 -n | cut -b 1)
    # $choice $resCat
case $choice in
    0)
        #resCat=$dir
        exit 1
        ;;
    1)
        emacsclient -c -a emacs $resCat
        ;;
    2)
        zathura $resCat
        ;;
    3)
        mv $resCat $(functio $home)
        ;;
    4)
        alacritty -e nvim $resCat
        ;;
    5)
        $resCat
        ;;

esac
fi
}
functio $home

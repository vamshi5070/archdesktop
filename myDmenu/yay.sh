choice=$(echo 1 | dmenu -p "Search: " -fn "SourceCodePro:size=34")
if [[ $choice = *[!\ ]* ]]; then
    alacritty -e yay $choice
fi
